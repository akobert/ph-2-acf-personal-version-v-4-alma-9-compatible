/*!
 *
 * \file CLASS_NAME_TEMPLATE.h
 * \brief CLASS_NAME_TEMPLATE class
 * \author CLASS_AUTOR
 * \date TODAY_DATE
 *
 */

#ifndef CLASS_NAME_TEMPLATE_h__
#define CLASS_NAME_TEMPLATE_h__

#include "tools/Tool.h"
#include <map>
#ifdef __USE_ROOT__
// Calibration is not running on the SoC: I need to instantiate the DQM histogrammer here
#include "DQMUtils/DQMHistogramCLASS_NAME_TEMPLATE.h"
#endif

class CLASS_NAME_TEMPLATE : public Tool
{
  public:
    CLASS_NAME_TEMPLATE();
    ~CLASS_NAME_TEMPLATE();

    void Initialise(void);

    // State machine
    void Running() override;
    void Stop() override;
    void ConfigureCalibration() override;
    void Pause() override;
    void Resume() override;
    void Reset();

    static std::string fCalibrationDescription;
    
  private:
    
#ifdef __USE_ROOT__
    // Calibration is not running on the SoC: Histogrammer is handeld by the calibration itself
    DQMHistogramCLASS_NAME_TEMPLATE fDQMHistogramCLASS_NAME_TEMPLATE;
#endif
};

#endif
